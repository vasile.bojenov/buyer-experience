/**
 * Utility function to run callbacks when targets enter the viewport
 *
 * @param {Array} targetIdentifiers - A list of identifiers to search the document for. Identifiers should be added to DOM elements with `data-slippers-scroll="identifier"`
 * @param {Function} callback - A function to call when a new DOM element enters the viewport.
 */

export function scrollActions(config, callback) {
  const defaultObserverOptions = {
    root: null,
    rootMargin: '0px',
    threshold: 1,
  };
  const targetIdentifiers = config.targetIdentifiers || [];
  const warningLabel = config.warningLabel || 'scrollActions';
  const observerOptions = config.observerOptions || defaultObserverOptions;
  /**
   * First, for each target string in `targetIdentifiers`, create an array of DOM elements that match `data-slippers-scroll="targetString"`
   * If nothing matches, throw a warning that we were provided a non-existent target. No need to bail out, just let us know.
   */
  const scrollTargets = [];
  for (let i = 0; i < targetIdentifiers.length; i++) {
    // eslint-disable-line no-plusplus
    const queryString = `[data-slippers-scroll="${targetIdentifiers[i]}"]`;
    const target = document.querySelector(queryString);
    if (target) {
      scrollTargets.push(target);
    } else {
      // eslint-disable-next-line no-console
      console.warn(
        `${warningLabel}: no element found that matches ${queryString}`,
      );
    }
  }

  /**
   * Then, for each one of those DOM elements, set up a new Intersection Observer: https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API
   *
   * Each intersection observer should use `null` as the intersection root, to use the document's viewport as a container.
   * Lets use 50% thresholds for now, but maybe we turn the first argument of this function into a configuration block, and take customizable thresholds.
   * When a target DOM element enters and crosses the 50% threshold, run its callback.
   */
  for (let j = 0; j < scrollTargets.length; j++) {
    // eslint-disable-line no-plusplus
    const observer = new IntersectionObserver(callback, observerOptions);
    observer.observe(scrollTargets[j]);
  }

  /**
   * The initial use case for this is that the `SlpEventNavigationDesktopMenu` component uses this scroll action
   * to update which  `slpEventNavigationLink` gets the `active` class applied to it - changing `active` to match whichever
   * target crosses the threshold.
   *
   * But more generally, the idea is to provide a utility that reports intersecting behaviors of a select list of
   * DOM elements.
   */
}
