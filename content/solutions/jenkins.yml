---
  title: GitLab Jenkins Integration
  description: GitLab's Jenkins integration allows effortlessly set up of your project to build with Jenkins, GitLab will output the results for you right from GitLab's UI.
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab Jenkins Integration
        subtitle: Trigger a Jenkins build for every push to your GitLab Projects
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          url: https://docs.gitlab.com/ee/integration/jenkins.html
          text: Documentation
          data_ga_name: jenkins integration
          data_ga_location: header
        # TODO: Change for actual image
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          image_url_mobile: /nuxt-images/solutions/no-image-mobile.svg          
          alt: "Image: gitlab jenkins integration"
    - name: copy
      data:
        block:
          - header: Overview
            text: |
              GitLab is a fully featured software development platform that includes, among other powerful [features](/features/){data-ga-name="features" data-ga-location="body"}, built-in [GitLab CI/CD](/stages-devops-lifecycle/continuous-integration/){data-ga-name="CI/CD" data-ga-location="body"} to leverage the ability to build, test, and deploy your apps without requiring you to integrate with CI/CD external tools.

              However, many organizations have been using [Jenkins](https://jenkins.io/) for their deployment processes, and need an integration with Jenkins to be able to onboard to GitLab before switching to GitLab CI/CD. Others have to use Jenkins to build and deploy their applications because of the inability to change the established infrastructure for current projects, but they want to use GitLab for all the other capabilities.

              With GitLab's Jenkins integration, you can effortlessly set up your project to build with Jenkins, and GitLab will output the results for you right from GitLab's UI.
    - name: copy-media
      data:
        block:
          - header: How it works
            inverted: true
            text: |
              * **Display Jenkins results on GitLab Merge Requests:** When you set up GitLab Jenkins integration to your project, any push to your project will trigger a build on the external Jenkins installation, and GitLab will output the pipeline status (success or failed) for you right on the merge request widget and from your project's pipelines list.
              * **Quickly access your build logs:** Every time you want to check your build log, you simply click on the result badge and GitLab will take you to your [pipeline](https://docs.gitlab.com/ee/ci/pipelines/index.html){data-ga-name="pipeline" data-ga-location="body"} on Jenkins UI.
            image:
              image_url: /nuxt-images/icons/checklist-icon.svg
              alt: ""
              svg_filter_style: "invert(30%) sepia(91%) saturate(1621%) hue-rotate(346deg) brightness(92%) contrast(91%)"
          - header: Benefits
            text: |
              * **Easily and quickly configurable:** Jenkins is easily integrated with [GitLab Enterprise Edition](/pricing/){data-ga-name="pricing" data-ga-location="body"}, right from your project's integrations settings. Once you've enabled the service to configure GitLab's authentication with your Jenkins server, and Jenkins knows how to interact with GitLab, it's ready to use, out-of-the-box.
              * **Maintain your workflow leveraged by GitLab:** despite having Jenkins running your builds, everything else can be handled by GitLab, since the discussion of new ideas, until the deployment to production. Using the Jenkins interface is needed only if you want to get more details, for example in case of failure.
            image:
              image_url: /nuxt-images/icons/clock-icon.svg
              alt: ""
              svg_filter_style: "invert(30%) sepia(91%) saturate(1621%) hue-rotate(346deg) brightness(92%) contrast(91%)"
