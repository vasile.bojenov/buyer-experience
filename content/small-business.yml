---
  title: "GitLab for Small Business - Collaboration made easy"
  description: "Accelerate your software delivery with GitLab’s DevOps platform, lowering your development costs and streamlining team collaboration"
  image_title: "/nuxt-images/open-graph/gitlab-smb-opengraph.png"
  canonical_url: "/small-business/"
  solutions_hero:
    title: GitLab for Small Businesses
    subtitle: One DevOps Platform to bring teams together, with everything you need built right in.
    header_animation: fade-down
    header_animation_duration: 800
    buttons_animation: fade-down
    buttons_animation_duration: 1200
    img_animation: zoom-out-left
    img_animation_duration: 1600
    primary_btn:
      text: Try For Free
      url: https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com
      data_ga_name: join gitlab
      data_ga_location: header
    image:
      image_url: /nuxt-images/small-business/small-business-header.jpg
      alt: ""
      rounded: true
  by_industry_intro:
    logos:
      - name: Hotjar
        image: /nuxt-images/logos/hotjar-logo.svg
        aos_animation: zoom-in-up
        aos_duration: 200
        url: /customers/hotjar/
        aria_label: Link to Hotjar customer case study
      - name: Chorus
        image: /nuxt-images/logos/chorus_color.svg
        aos_animation: zoom-in-up
        aos_duration: 400
        url: /customers/chorus/
        aria_label: Link to Chorus customer case study
      - name: Anchormen
        image: /nuxt-images/logos/anchormen-logo.svg
        aos_animation: zoom-in-up
        aos_duration: 600
        url: /customers/anchormen/
        aria_label: Link to Anchormen customer case study
      - name: Remote
        image: /nuxt-images/logos/remote-logo.svg
        aos_animation: zoom-in-up
        aos_duration: 800
        url: /customers/remote/
        aria_label: Link to Remote customer case study
      - name: Glympse
        image: /nuxt-images/logos/glympse-logo.svg
        aos_animation: zoom-in-up
        aos_duration: 1000
        url: /customers/glympse/
        aria_label: Link to Glympse customer case study
      - name: MGA
        image: /nuxt-images/logos/mga-logo.svg
        aos_animation: zoom-in-up
        aos_duration: 1200
        url: /customers/mga/
        aria_label: Link to M.G.A customer case study
  by_solution_intro:
    aos_animation: fade-up
    aos_duration: 800
    text:
      highlight: Small businesses have so much to do.
      description: What worked well for individual projects will be difficult to scale across the enterprise. DevOps solutions shouldn’t create more problems than they solve. Unlike brittle toolchains built on point solutions, GitLab lets teams iterate faster and innovate together, removing complexity and risk, providing everything you need to deliver higher quality, more secure software faster.
  by_solution_benefits:
    title: DevOps at scale
    is_accordion: true
    right_block_animation: zoom-in-left
    right_block_duration: 800
    left_block_animation: zoom-in-right
    left_block_duration: 800
    header_animation: fade-up
    header_duration: 800
    image:
      image_url: /nuxt-images/enterprise/enterprise-devops-at-scale.jpg
      alt: ""
    items:
      - icon: /nuxt-images/solutions/benefits/continuous-integration.svg
        header: Get started faster
        text: Everything you need, out of the box, to manage all of your DevOps processes in one place, with templates to get started quickly and best practices built-in.
      - icon: /nuxt-images/solutions/benefits/agile.svg
        header: Simplify DevOps
        text: Teams need to focus on delivering value — not maintaining toolchain integrations. 
      - icon: /nuxt-images/icons/value-prop/auto-scale.svg
        header: Enterprise-ready
        text: As your business scales, so does your DevOps platform — without scaling your complexity.
      - icon: /nuxt-images/icons/value-prop/devsecops.svg
        header: Reduce risk and cost
        text: Automate and enforce security and compliance without compromising speed or spend.
  by_industry_solutions_block:
    subtitle: Key Capabilities
    sub_description: "The One DevOps Platform provides true end-to-end support to help you deliver maximum customer value with minimum friction. Key capabilities include:"
    white_bg: true
    markdown: true
    sub_image: /nuxt-images/small-business/no-image-alternative-export.svg
    solutions:
      - title: GitLab Free 
        description: |
          **Automated Software Delivery**

          DevOps essentials of SCM, CI, CD, GitOps in one easy-to-use platform
          &nbsp;

          **Basic issue management**

          Create issues (aka stories), assign them and track their progress
          &nbsp;

          **Basic security scanning**

          Static Application Security Testing (SAST) and Secrets detection        
        link_text: Learn More
        link_url: /pricing
        data_ga_name: gitlab free
        data_ga_location: body
      - title: GitLab Premium
        description: |
          **Automated Software Delivery**

          DevOps essentials of SCM, CI, CD, GitOps in one easy-to-use platform with added management features
          &nbsp;

          **Basic issue management**

          Create issues (aka stories) and epics, assign them, and track their progress.
          &nbsp;

          **Basic security scanning**
          
          Static Application Security Testing (SAST) and secrets detection.
        link_text: Learn More
        link_url: /pricing/
        data_ga_name: gitlab premium
        data_ga_location: body
      - title: GitLab Ultimate
        description: |
          **Automated software delivery**
          
          DevOps essentials of SCM, CI, CD, and GitOps in one easy-to-use platform with comprehensive management features to help you scale.
          &nbsp;
          
          **Agile planning**
          
          Project planning with issues, multi-level epics, burn-down charts and more.
          &nbsp;
          
          **Comprehensive security testing** 
          
          Comprehensive application security testing including SAST, secrets detection, DAST, containers, dependencies, cluster images, APIs, fuzz testing, and license compliance.
          &nbsp;
          
          **Vulnerability management**
          
          See security and compliance flaws in actionable dashboards for vulnerability assessment, triage, and remediation.
          &nbsp;
          
          **Governance**
          
          Compliance pipelines to automate policy and security guardrails.
          &nbsp;
          
          **Value stream management**
          
          End-to-end metrics to help you improve your software development velocity and outcomes.
        link_text: Learn More
        link_url: /pricing/
        data_ga_name: gitlab ultimate
        data_ga_location: body
  by_solution_value_prop:
    title: One Platform for Dev, Sec, and Ops
    header_animation: fade-up
    header_animation_duration: 500
    cards_animation: zoom-in-up
    cards_animation_duration: 500
    cards:
      - title: SCM
        description: Source code management for version control, collaboration, and basic story planning.
        icon: /nuxt-images/icons/value-prop/cog-code.svg
      - title: CI/CD
        description: Continuous integration and delivery with Auto DevOps. 
        icon: /nuxt-images/icons/value-prop/continuous-delivery.svg
      - title: GitOps
        description: Infrastructure automation to abstract the complexities of cloud native environments.
        icon: /nuxt-images/solutions/automated-code.svg
      - title: Security
        description: Comprehensive security scanning and vulnerability management — ready when you are.
        icon: /nuxt-images/icons/value-prop/sheild-check.svg
  by_industry_case_studies:
    title: Customer Realized Benefits
    charcoal_bg: true
    header_animation: fade-up
    header_animation_duration: 500
    row_animation: fade-right
    row_animation_duration: 800
    rows:
      - title: Anchormen
        subtitle: How GitLab CI/CD supports and accelerates innovation for Anchormen
        image:
          url: /nuxt-images/blogimages/anchormen.jpg
          alt: 
        button:
          href: /customers/anchormen/
          text: Learn more
          data_ga_name: anchormen learn more
          data_ga_location: body
      - title: Glympse
        subtitle: Glympse is making geo-location sharing easy
        image:
          url: /nuxt-images/blogimages/glympse_case_study.jpg
          alt: 
        button:
          href: /customers/glympse/
          text: Learn more
          data_ga_name: glympse learn more
          data_ga_location: body
      - title: X-Cite
        subtitle: How X-Cite uses GitLab’s SCM for robust worldwide workflow
        image:
          url: /nuxt-images/blogimages/xcite_cover_image.jpg
          alt: 
        button:
          href: /customers/xcite/
          text: Learn more
          data_ga_name: xcite learn more
          data_ga_location: body
      - title: MGA
        subtitle: How MGA builds projects 5 times faster with GitLab
        image:
          url: /nuxt-images/blogimages/covermga.jpg
          alt: 
        button:
          href: /customers/mga/
          text: Learn more
          data_ga_name: mga learn more
          data_ga_location: body
      - title: Hotjar
        subtitle: How Hotjar deploys 50% faster with GitLab
        image:
          url: /nuxt-images/blogimages/hotjar.jpg
          alt: 
        button:
          href: /customers/hotjar/
          text: Learn more
          data_ga_name: hotjar learn more
          data_ga_location: body
      - title: Nebulaworks
        subtitle: How Nebulaworks replaced 3 tools with GitLab and empowered customer speed and agility
        image:
          url: /nuxt-images/blogimages/nebulaworks.jpg
          alt: 
        button:
          href: /customers/nebulaworks/
          text: Learn more
          data_ga_name: nabulaworks learn more
          data_ga_location: body
  solutions_resource_cards:
    column_size: 4
    cards:
      - icon: /nuxt-images/home/resources/icon_ebook.svg
        event_type: Ebook
        header: An SMB Guide to Getting Started in DevOps
        link_text: Read more
        image: /nuxt-images/blogimages/vlabsdev_coverimage.jpg
        href: https://page.gitlab.com/resources-ebook-smb-beginners-guide-devops.html
        aos_animation: fade-up
        aos_duration: 400
      - icon: /nuxt-images/home/resources/icon_blog.svg
        event_type: Blog Post
        header: 6 ways SMBs can leverage the power of a DevOps platform
        link_text: Read more
        image: /nuxt-images/blogimages/shahadat-rahman-gnyA8vd3Otc-unsplash.jpg
        href: https://about.gitlab.com/blog/2022/04/12/6-ways-smbs-can-leverage-the-power-of-a-devops-platform/
        aos_animation: fade-up
        aos_duration: 600
      - icon: /nuxt-images/home/resources/icon_blog.svg
        event_type: Learn
        header: Leading SCM, CI and Code Review in one application
        link_text: Read more
        image: /nuxt-images/blogimages/zoopla_cover_image.jpg
        href: https://learn.gitlab.com/smb-ci-1/leading-scm-ci-and-c
        aos_animation: fade-up
        aos_duration: 800
      - icon: /nuxt-images/features/resources/icon_video.svg
        event_type: video
        header: Watch a GitLab DevOps Platform Demo
        link_text: Read more
        image: /nuxt-images/resources/fallback/img-fallback-cards-devops.png
        href: https://learn.gitlab.com/smb-beginners-devops/oei67xcnxmk
        aos_animation: fade-up
        aos_duration: 1000
      - icon: /nuxt-images/features/resources/icon_case_study.svg
        event_type: Blog
        header: Can an SMB or start-up be too small for a DevOps platform?
        link_text: Read more
        image: /nuxt-images/features/resources/resources_case_study.png
        href: https://about.gitlab.com/blog/2022/04/06/can-an-smb-or-start-up-be-too-small-for-a-devops-platform/
        aos_animation: fade-up
        aos_duration: 1200