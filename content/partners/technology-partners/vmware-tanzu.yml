---
  title: GitLab on VMware Tanzu
  description: Empower your team to continuously deliver better code with the DevOps platform to plan, build, test, and deploy on VMware Tanzu.
  components:
    - name: partners-call-to-action
      data:
        title: GitLab on VMware Tanzu
        inverted: true
        body:
          - text: Empower your team to continuously deliver better code with the DevOps platform to plan, build, test, and deploy on VMware Tanzu.
        image:
          image_url: /nuxt-images/partners/vmware/vmware_tanzu_vertical.png
          alt: VMWare Tanzu Logo
    - name: copy-media
      data:
        block:
            - header: Run anywhere together
              text: |
                Develop cloud native applications for the VMware Tanzu platform. Free workloads from infrastructure to work independently and run anywhere with GitLab – a single solution for everyone on your pipeline. Build, secure, deploy, and migrate applications to VMware services via a complete DevSecOps self-service model with GitLab and Tanzu. The finish line: digital transformation. GitLab gets you there.
              image:
                image_url: /nuxt-images/partners/vmware/featured-partner-vmware-tanzu-1.png
              miscellaneous: Bronwyn Hastings, VP Technology Ecosystem on Google’s partnership with GitLab to deliver digital transformations for customers.
              link_href: https://tanzu.vmware.com/developer/guides/ci-cd/gitlab-ci-cd-cnb/
              link_text: Read the blog
              secondary_link_href: /free-trial/
              secondary_link_text: Start your free trial
    - name: 'partners-feature-showcase'
      data:
        header: Develop better cloud native applications faster with GitLab and VMware Tanzu
        image_url: /nuxt-images/partners/vmware/featured-partner-vmware-tanzu-2.svg
        text: Reduce friction across teams and workflows to compress cycle times with GitLab’s consistent, scalable interface. Iterate faster and innovate together with built-in planning, monitoring, and reporting solutions plus tight VMware Tanzu integration.
        cards:
          - header: One-for-all collaboration
            text: Optimize contributions. From issue tracking to code review, GitLab reduces rework. Happier developers expand product roadmaps instead of repairing old roads.
          - header: Unshakable automation
            text: Deploy unstoppable software with DevSecOps. Automated workflows increase uptime by reducing security and compliance risks on VMware Tanzu.
          - header: Countless wins
            text: Mature, profit, repeat. Grow your market share and revenue when you continuously deliver on-budget, on-time, and always-up products.
    - name: 'benefits'
      data:
        header: Get started with GitLab and VMware Tanzu Joint Solutions
        description: GitLab is a featured DevOps solution in the [VMware Tanzu Marketplace](https://tanzu.vmware.com/solutions-hub/devops-tooling/gitlab). As an open core platform, GitLab integrates with your current processes so you can adopt an end-to-end software delivery lifecycle while maintaining the investment in your current toolchain. Leverage GitLab and VMware Tanzu joint solutions to create a continuous integration (CI) pipeline with Kubernetes, enable Continuous Verification, and more.
        full_background: true
        cards_per_row: 2
        text_align: left
        benefits:
          - icon: /nuxt-images/partners/vmware/TZLG-VMwareTanzu-icon.png
            title: VMware Tanzu Application Service (TAS)
            description: Leverage automated workflows with GitLab CI/CD to deploy ava, .NET, and Node applications on TAS. GitLab on TAS is a joint solution for cloud native development, legacy app migration, and modernization.
            link:
              text: Learn more
              url: https://tanzu.vmware.com/application-service
          - icon: /nuxt-images/partners/vmware/kubernetes-logo-color.svg
            title: VMware Tanzu Kubernetes Grid (TKGI) validated
            description: Helm chart delivers GitLab to VMware-based Kubernetes clusters with VMware Tanzu Kubernetes Grid (TKGI). Visit GitLab through the webUI and interact with it via the standardized Git command-line application. GitLab runs on TKGI with no dependency on TAS.
            link:
              text: Learn more
              url: https://tanzu.vmware.com/kubernetes-grid
          - icon: /nuxt-images/partners/vmware/TZLG-VMwareTanzu-icon.png
            title: VMware Tanzu Observability by Wavefront
            description: Get 3D observability with the Wavefront GitLab integration, featuring a GitLab dashboard for HTTP, process, and network stats. Fetch and push GitLab metrics to Wavefront by installing the Telegraf Agent and enabling the Prometheus Input Plugin.
            link:
              text: Learn more
              url: https://tanzu.vmware.com/observability
          - icon: /nuxt-images/partners/vmware/cloud-health.png
            title: CloudHealth and GitLab
            description: Together, GitLab CI/CD and CloudHealth provide Day-2 operations for budget and resource management with easy access to insights via an API integration. Enable project-specific pipeline checks and governance rules to reduce the footprint of your deployments.
            link:
              text: Learn more
              url: https://www.cloudhealthtech.com/
          - icon: /nuxt-images/partners/vmware/cloud-assembly.png
            title: GitLab Integration in Cloud Assembly
            description: Harness GitLab CI/CD iterative workflows to develop Infrastructure as Code (IaC) and deploy to your public and private cloud providers with GitLab in Cloud Assembly. Enable blueprint authoring to directly commit changes to blueprints repositories stored in GitLab and synchronize into VMware Cloud Assembly projects with automated tasks.
            link:
              text: Learn more
              url: https://www.vmware.com/products/vrealize-automation.html
    - name: 'pull-quote'
      data:
        quote: Enabling automated deployments with verified Bitnami images and codified policies enables a Continuous Verification process which can reduce costs, security risks, and potential performance issues. We’re excited to work with partners such as GitLab to empower customers to fully leverage their cloud investments.
        source: MILIN DESAI, GM, CLOUD SERVICES AT VMWARE
        link_text: ''
        shadow: true
    - name: copy-resources
      data:
        title: Discover the benefits of GitLab on VMware
        block:
          - video:
              title: 'Opening Keynote: The Power of GitLab - Sid Sijbrandij'
              video_url: https://www.youtube.com/embed/xn_WP4K9dl8?enablesjsapi=1
              label: Gitlab commit virtual 2020
            resources:
              blog:
                header: Blogs
                links:
                  - text: GitLab Tanzu Services Marketplace Listing
                    link: https://tanzu.vmware.com/solutions-hub/devops-tooling/gitlab
                  - text: VMworld-2020-demo/spring-music demo
                    link: https://gitlab.com/gitlab-com/alliances/vmware/sandbox/vmworld-2020-demo/spring-music
                  - text: GitLab to Enable Cloud Native Transformation on VMware Cloud Marketplace
                    link: https://www.globenewswire.com/news-release/2019/09/17/1916738/0/en/GitLab-to-Enable-Cloud-Native-Transformation-on-VMware-Cloud-Marketplace.html
                  - text: Webinar - Cloud Native
                    link: https://tanzu.vmware.com/content/webinars/jul-30-best-practices-for-cloud-native-pipelines-with-gitlab-and-vmware-tanzu?utm_campaign=Global_BT_Q221_Best-Practices-Cloud-Native-Pipeline-Gitlab&utm_source=sales-email&utm_medium=partner
